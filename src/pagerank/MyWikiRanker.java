/**
 * 
 */
package pagerank;

/**
 * @author Revanth
 * @author Vamsi Krishna Jagarlamudi
 *
 */
public class MyWikiRanker {
	
	public static void main(String args[])
	{
		String fileName = "MyWikiRanker.txt";
		
		String[] topics = {"ball", "team", "kick", "game", "soccer"};
		
		WikiCrawler wc = new WikiCrawler("/wiki/Football", topics, 100, fileName, true);
		wc.crawl();
		
		System.out.println("####### approx = " + "0.05" + " #######");
		PageRank pageRank = new PageRank(fileName,0.05d);
		
		// Print Page Ranks for Top 10
		Util.printTopK(pageRank, 10);
	}
}
