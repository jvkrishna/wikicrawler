package pagerank;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Util {

	public static final double BETA = 0.85d;
	public static final Pattern TAG_PATTERN = Pattern.compile("(<(?!a|\\/a>)(?=[a-zA-Z\\/]+).*?>)");
	public static final Pattern A_TAG_PATTERN = Pattern.compile("(?i)<a([^>]+)>(.+?)</a>");
	public static final Pattern HREF_TAG_PATTERN = Pattern
			.compile("\\s*(?i)href\\s*=\\s*(\"([^\"]*\")|'[^']*'|([^'\">\\s]+))");

	/**
	 * Constructs WebGraph from the file.
	 * @param filePath
	 * @return
	 */
	public static WebGraph constructWebGraphFromFile(Path filePath) {
		try (BufferedReader br = new BufferedReader(new FileReader(filePath.toFile()))) {
			int v = Integer.parseInt(br.readLine());
			WebGraph wg = new WebGraph(v);
			String line = null;
			GraphNode srcNode, destNode;
			while ((line = br.readLine()) != null) {
				String[] nodes = line.split("\\s+");
				String src = nodes[0], dest = nodes[1];
				srcNode = new GraphNode(src, 1 / (double) v);
				destNode = new GraphNode(dest, 1 / (double) v);
				wg.addEdge(srcNode, destNode);
			}

			return wg;

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Iteratively calculates the page rank using the random surfer model.
	 * @param wg
	 * @param approx
	 * @return
	 */
	public static Map<String, Double> computePageRanks(WebGraph wg, double approx) {
		Map<String, Double> prev = wg.getNodesMap().entrySet().stream()
				.collect(Collectors.toMap(Map.Entry::getKey, e -> e.getValue().getWeight()));
		double norm;
		Map<String, Double> next;
		do {
			next = nextPageRankVector(prev, wg);
			norm = norm(prev, next);
			if (norm <= approx)
				break;
			prev = next;
		} while (norm > approx);
		return prev;

	}

	/**
	 * Updates the page rank as the weight in the web graph.
	 * @param rankMap
	 * @param wg
	 */
	public static void updatePageRanksAsWeight(Map<String, Double> rankMap, WebGraph wg) {
		for (GraphNode node : wg.getNodesMap().values()) {
			node.setWeight(rankMap.get(node.getNodeName()));
		}
	}

	/**
	 * Generates the next page rank using the random walk algorithm.
	 * @param prev
	 * @param wg
	 * @return
	 */
	public static Map<String, Double> nextPageRankVector(Map<String, Double> prev, WebGraph wg) {
		// Next vector initialization.
		Map<String, Double> next = new HashMap<>();
		wg.getNodesMap().forEach((k, v) -> {
			next.put(k, (1 - BETA) / wg.getV());
		});
		for (Entry<String, GraphNode> es : wg.getNodesMap().entrySet()) {
			GraphNode node = es.getValue();
			List<GraphNode> outNodes = node.getEdges();
			if (!outNodes.isEmpty()) {
				for (GraphNode outNode : outNodes) {
					double newRank = next.get(outNode.getNodeName())
							+ (BETA * prev.get(node.getNodeName()) / outNodes.size());
					next.put(outNode.getNodeName(), newRank);
				}
			} else {
				wg.getNodesMap().keySet().forEach(nodeName -> {
					double newRank = next.get(nodeName) + (BETA * prev.get(node.getNodeName()) / wg.getV());
					next.put(nodeName, newRank);
				});
			}
		}
		return next;
	}

	/**
	 * Calculates the L1-Norm .
	 * @param prev
	 * @param next
	 * @return
	 */
	public static double norm(Map<String, Double> prev, Map<String, Double> next) {
		double norm = 0;
		for (Entry<String, Double> prevEntrySet : prev.entrySet()) {
			norm += Math.abs(prevEntrySet.getValue() - next.get(prevEntrySet.getKey()));
		}
		return norm;
	}

	/**
	 * Calculates the jaccard similarity.
	 * @param s1
	 * @param s2
	 * @return
	 */
	public static double jaccardSimilarity(Set<String> s1, Set<String> s2) {
		long intersection = s1.stream().filter(s -> s2.contains(s)).count();
		long union = s1.size() + s2.size() - intersection;
		return (double) intersection / union;

	}
	
	/*
	 * This function takes in a URL, open a stream to the URL and starts reading the repsonse.
	 */
	public static BufferedReader readURL(String httpUrl) throws IOException {
		URL url = new URL(httpUrl);
		InputStream is = url.openStream();
		return new BufferedReader(new InputStreamReader(is));

	}
	
	/*
	 * This method gets the HTML page from a URL, creates a list of all those HTML links in that Page and also a list of 
	 * all the positions that contain all the key words in the page.
	 */
	public static void getWordsAndLinks(String httpUrl, String[] keywords, List<HTMLLink> links,
			List<Integer> positions) {
		try (BufferedReader br = readURL(httpUrl)) {
			String line = "";
			// Don't process till the first p tag.
			while ((line = br.readLine()) != null) {
				if (line.contains("<p>"))
					break;
			}
			int pos = 0;
			do {
				line = line.replaceAll(TAG_PATTERN.pattern(), "");
				Matcher linkMatcher = A_TAG_PATTERN.matcher(line);
				int start = 0;
				while (linkMatcher.find()) {
					// Substring processing.
					String subStr = line.substring(start, linkMatcher.start());
					String[] words = subStr.split("\\s+");
					for (String word : words) {
						if (Arrays.stream(keywords).anyMatch(w -> word.toLowerCase().contains(w.toLowerCase())
								|| w.toLowerCase().contains(word.toLowerCase()))) {
							positions.add(pos);
						}
						pos++;
					}
					start = linkMatcher.end();

					// Link Processing
					String href = linkMatcher.group(1);
					String linkText = linkMatcher.group(2);
					Matcher matcher = HREF_TAG_PATTERN.matcher(href);
					while (matcher.find()) {
						String link = matcher.group(1).replaceAll("\"", "");
						if (allowLink(link)) {
							links.add(new HTMLLink(link, linkText, pos));
						}
					}
					pos++;

				}
				// remaining text
				if (start < line.length() - 1) {
					String subStr = line.substring(start, line.length());
					String[] words = subStr.split("\\s+");
					for (String word : words) {
						if (Arrays.stream(keywords).anyMatch(w -> word.toLowerCase().contains(w.toLowerCase())
								|| w.toLowerCase().contains(word.toLowerCase()))) {
							positions.add(pos);
						}
						pos++;
					}
				}

			} while ((line = br.readLine()) != null);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	/*
	 * This function takes in a list of all HTML links and all the positions where the key words are found.
	 * Creates a GraphNode for each link and calculate the weight for that node(link) and return a List of all the GraphNodes
	 * for the HTML links.    
	 */
	public static List<GraphNode> createGraphNodes(String[] topics, List<HTMLLink> links, List<Integer> positions,
			boolean isWeighted) {
		
		//Set to make sure that there are no duplicate graph Nodes
		Set<GraphNode> graphNodesTemp = new HashSet<GraphNode>();

		//Final list of unique graph Nodes
		List<GraphNode> graphNodes = new ArrayList<>();
		
		//For each HTML link 
		for (int i = 0; i < links.size(); i++) {
			HTMLLink linkObj = links.get(i);
			int distance;
			GraphNode node = new GraphNode(linkObj.link);
			
			/*
			 * Check if the link or the link text contains one of the key works or if we don't want to do a weighted BFS.
			 * In either one of these cases, the weight would be set 1 for that particular GraphNode
			 * Else, we calculate the weight and set the weight in the GraphNode.
			 */
			if (!isWeighted || Arrays.stream(topics).parallel().anyMatch(linkObj.link.toLowerCase()::contains)
					|| Arrays.stream(topics).parallel().anyMatch(linkObj.Text.toLowerCase()::contains)) {
				node.setWeight(1d);
			}
			
			//Calculates the weight for the link using the positions where the key words occur
			else {
				int nearestPos = -1 - Collections.binarySearch(positions, linkObj.position);

				if (nearestPos > 0)
					distance = Integer.min(linkObj.position - positions.get((nearestPos - 1)),
							positions.get(nearestPos) - linkObj.position);
				else
					distance = linkObj.position - positions.get(0);
				node.setWeight(distance > 20 ? 0 : 1 / (double) (distance + 2));

			}
			
			//If the graphNode does not exist in the set, then we add that Graph Node to the list and the set.
			if(!graphNodesTemp.contains(node))
			{
				graphNodes.add(node);
				graphNodesTemp.add(node);
			}
		}

		return graphNodes;
	}
	
	/*
	 * This function creates a set of all the disallowed links mentioned in Robots.txt 
	 */
	public static Set<String> disallowedSites() {
		Set<String> disallowedSites = new HashSet<>();

		try (BufferedReader br = readURL(WikiCrawler.BASE_URL + "/robots.txt")) {
			String line;
			// Ignore till user-agent : *
			while ((line = br.readLine()) != null) {
				if (line.equalsIgnoreCase("User-agent: *"))
					break;
			}
			while ((line = br.readLine()) != null) {
				if (line.startsWith("Disallow:")) {
					disallowedSites.add(line.split("\\s+")[1].trim());
				}
			}

		} catch (IOException e) {
			System.out.println("Unable to capture the robots.txt fie.");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return disallowedSites;
	}
	
	//This function takes in WebGraph and traverses the WebGraph using BFS and writes all the edges to a file
	public static void weightedBFSTraverse(WebGraph wg, GraphNode root, int maxNodes, String filePath) {
		
		Set<GraphNode> visitedNodes = new HashSet<>();
		
		//Main WeightedQ
		WeightedQ wq = new WeightedQ();
		wq.add(root);
		
		visitedNodes.add(root);
		GraphNode node;
		
		try (BufferedWriter bw = new BufferedWriter(new FileWriter(filePath))) {
			
			bw.write(maxNodes + "");
			bw.newLine();
			
			/*
			 * For each node in the Main Queue, we get all its child nodes and place them in in a temporary Weighted queue. 
			 * Then we extract each node from the Weighted Queue and write the edge from the parent node to the child node.
			 * This way Child node that has a higher weight will be added to the file first. Then we add the child node to the 
			 * Main weighted queue and move to the next node.
			 */ 
			
			while ((node = wq.extract()) != null && visitedNodes.size() <= maxNodes) 
			{
				WeightedQ tmpWQ = new WeightedQ();
				node.getEdges().forEach(e -> tmpWQ.add(e));
				GraphNode childNode = null;
				while ((childNode = tmpWQ.extract()) != null) {
					if (!visitedNodes.contains(childNode)) {
						wq.add(childNode);
						visitedNodes.add(childNode);
					}
					if (visitedNodes.size() <= maxNodes) {
						bw.write(node.getNodeName() + " " + childNode.getNodeName());
						bw.newLine();
					}
				}
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	//Checks if a specific link should be crawled or not.
	public static boolean allowLink(String link) {
		if (!link.contains("#") && !link.contains(":") && link.startsWith("/wiki/")) {

			return WikiCrawler.disAllowedLinks.stream().noneMatch(s -> s.equals(link));

		}

		return false;

	}

	public static void printTopK(PageRank pageRank, int k) {

		System.out.println("Top " + k + " Page Ranks");
		Arrays.stream(pageRank.topKPageRank(k)).forEach(l -> System.out.println(l + " : " + pageRank.pageRankOf(l)));
		System.out.println("-----------------------");
		System.out.println("Top " + k + " Page In Degrees");
		Arrays.stream(pageRank.topKInDegree(k)).forEach(l -> System.out.println(l + " : " + pageRank.inDegreeOf(l)));
		System.out.println("-----------------------");
		System.out.println("Top " + k + " Page Out Degrees");
		Arrays.stream(pageRank.topKOutDegree(k)).forEach(l -> System.out.println(l + " : " + pageRank.outDegreeOf(l)));
		System.out.println("-----------------------");
	}

	public static void printJaccardSimilarity(PageRank pageRank, int k) {
		Set<String> topKRanks = Arrays.stream(pageRank.topKPageRank(k)).collect(Collectors.toSet());
		Set<String> topKIns = Arrays.stream(pageRank.topKInDegree(k)).collect(Collectors.toSet());
		Set<String> topkOuts = Arrays.stream(pageRank.topKOutDegree(k)).collect(Collectors.toSet());

		System.out.println("Jaccard Similarity between Page Ranks and In Degrees ");
		System.out.println(Util.jaccardSimilarity(topKRanks, topKIns));
		System.out.println("------------------------");

		System.out.println("Jaccard Similarity between Page Ranks and Out Degrees ");
		System.out.println(Util.jaccardSimilarity(topKRanks, topkOuts));
		System.out.println("------------------------");

		System.out.println("Jaccard Similarity between In Degrees and Out Degrees");
		System.out.println(Util.jaccardSimilarity(topKIns, topkOuts));
		System.out.println("------------------------");
	}

}
