package pagerank;

/**
 * This class is a Data Structure used to implement the Webgraph
 */

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class WebGraph {
	/**
	 * Attributes in the Data Structure
	 */
	private int v;
	private Map<String, GraphNode> nodesMap;

	/**
	 * Constructor
	 * @param v
	 */
	public WebGraph(int v) {
		this.v = v;
		this.nodesMap = new HashMap<>(v);
	}
	
	/**
	 * This function is used to add an edge from node to another node.
	 * @param src
	 * @param dest
	 */
	public void addEdge(GraphNode src, GraphNode dest) {
		
		//The two lines below add the source and destination to the WebGraph if that Node is not present
		this.nodesMap.putIfAbsent(src.getNodeName(), src);
		this.nodesMap.putIfAbsent(dest.getNodeName(), dest);
		
		//Adds the destination node to to the set of nodes that the source node has edges to.
		this.nodesMap.get(src.getNodeName()).getEdges().add(dest);
		
		//Increments the in degree of the destination node.
		this.nodesMap.get(dest.getNodeName()).incrementInDegree();
	}
	
	/*
	 * Returns the number of vertices in the WebGraph.
	 * @return
	 */
	
	public int getV() {
		return v;
	}
	
	/*
	 * Returns a Map of all the nodes and their edges
	 * @return
	 */
	public Map<String, GraphNode> getNodesMap() {
		return this.nodesMap;
	}

	/*
	 * Returns the set of nodes in the WebGraph. 
	 */
	public Set<GraphNode> getNodeSet() {
		return new HashSet<>(this.nodesMap.values());
	}

}
