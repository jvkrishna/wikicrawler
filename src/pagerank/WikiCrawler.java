/**
 * 
 */
package pagerank;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author revanth
 *
 */
public class WikiCrawler {
	
	/*
	 * All the attributes in the class
	 */
	public static final String BASE_URL = "https://en.wikipedia.org";
	public static Set<String> disAllowedLinks = new HashSet<String>();
	public String seedURL = "";
	public String[] topics;
	public int max;
	public String fileName;
	public boolean isWeighted;
	Map<String, ArrayList<Integer>> wordmap = new HashMap<String, ArrayList<Integer>>();
	int index;
	
	/*
	 * Constructor
	 */
	public WikiCrawler(String seedURL, String[] topics, int max, String fileName, boolean isWeighted) {
		super();
		this.seedURL = seedURL;
		this.topics = topics;
		this.max = max;
		this.fileName = fileName;
		this.isWeighted = isWeighted;
		this.index = 0;
		disAllowedLinks = Util.disallowedSites();
	}
	
	/*
	 * This Method crawls all the respective pages and creates a WebGraph 
	 */
	public void crawl() {
		
		//Checks if the seedURL is allowed to be crawled or not
		if (disAllowedLinks.contains(this.seedURL)) {
			System.out.println("Seed URL is not allowed by Robots.txt");
			System.exit(0);
		}
		
		
		WebGraph wg = new WebGraph(this.max);
		GraphNode root = new GraphNode(this.seedURL);
		GraphNode parent = root;
		WeightedQ wq = new WeightedQ();
		int counter = 0;
		Set<GraphNode> visitedNodes = new HashSet<>();
		visitedNodes.add(parent);
		
		/*
		 * For each node in the queue, get all the links and position of key words. Create GraphNodes.
		 * Check if it not the same as the parent and add a edge from the parent to the child in the WebGraph if that edge is not already present in the WebGraph.
		 * Then add the child node to the Weighed Queue. Once maximum number of nodes have been created in the WebGraph, 
		 * then for each node in the queue, we retrieve the page and add a link from that page to it's child node if the child node is already in the graph.
		 */
		do {

			// Prepare links and words
			List<HTMLLink> htmlLinks = new ArrayList<>();
			List<Integer> positions = new ArrayList<>();
			
			Util.getWordsAndLinks(BASE_URL + parent.getNodeName(), topics, htmlLinks, positions);
			counter++;

			// Get list of children.
			List<GraphNode> childNodes = Util.createGraphNodes(this.topics, htmlLinks, positions, isWeighted);
			
			
			for (GraphNode childNode : childNodes) {
				if (!childNode.equals(parent)) {
					if (visitedNodes.size() < max) {
						wg.addEdge(parent, childNode);
						if (!visitedNodes.contains(childNode)) {
							wq.add(childNode);
							visitedNodes.add(childNode);
						}
					} else {
						if (wg.getNodesMap().containsKey(childNode.getNodeName())) {
							wg.addEdge(parent, childNode);
						}
					}
				}
			}

			parent = wq.extract();

			// Sleep if more than 10 requests sent
			if (counter >= 10) {
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				counter = 0;
			}

		} while (parent != null);

		// Crawl web graph using the weighted BFS and write content to the file.
		Util.weightedBFSTraverse(wg, root, this.max, this.fileName);
	}

}
