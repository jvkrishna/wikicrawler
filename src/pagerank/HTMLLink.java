package pagerank;
/**
 * This class is a Data Structure to store a HTML Link
 * @author revanth
 * @author Vamsi Krishna Jagarlamudi
 *
 */
public class HTMLLink {
	
	/**
	 * Different attributes in the data structure.
	 */
	public String link;
	public String Text;
	public int position;
	
	/**
	 * Constructor
	 * @param link
	 * @param linkText
	 * @param index
	 */
	
	public HTMLLink(String link, String linkText, int index) {
		super();
		this.link = (link != null ? link.replace("\"", "") : "");
		this.Text = linkText;
		this.position = index;
	}
}
