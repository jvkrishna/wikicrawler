package pagerank;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Vamsi Krishna J
 * @author Revanth A
 *
 *This class is a data structure for each Node in the Web graph.
 */
public class GraphNode implements Comparable<GraphNode> {
	
	//All the attributes in the data structure.
	private String nodeName;
	private double weight;
	private List<GraphNode> edges;
	private int indegree = 0;
	private long timestamp;
	
	//Constructor
	public GraphNode(String nodeName) {
		this(nodeName, 0);
	}
	
	//Constructor
	public GraphNode(String nodeName, double weight) {
		this.nodeName = nodeName;
		this.weight = weight;
		this.edges = new ArrayList<>();
		this.timestamp = System.nanoTime();
	}
	
	/**
	 * Getter and Setter methods for all the attributes in the Structure.
	 */
	public String getNodeName() {
		return nodeName;
	}

	public void setNodeName(String nodeName) {
		this.nodeName = nodeName;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public List<GraphNode> getEdges() {
		return edges;
	}

	public void setEdges(List<GraphNode> edges) {
		this.edges = edges;
	}

	public int getIndegree() {
		return indegree;
	}

	public void incrementInDegree() {
		this.indegree++;
	}
	
	/**
	 * This method compares two GraphNodes and tells us if this object is is before or after an other element 
	 * when placed in a WeightedQ.
	 * It initially compares their weights and if their weights are the same, it then compares the timestamp
	 * they were created and returns the respective value.
	 */
	@Override
	public int compareTo(GraphNode o) {
		if(o.weight == this.weight) {
			return Long.compare(this.timestamp, o.timestamp);
		} else {
			return Double.compare(o.weight, this.weight);
		}
	}
	
	/** 
	 * 	The hashCode() and the equals() methods are implemented using the nodename attribute so
		that we can check if two Grpah Node objects are equal or not
	*/ 
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nodeName == null) ? 0 : nodeName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GraphNode other = (GraphNode) obj;
		if (nodeName == null) {
			if (other.nodeName != null)
				return false;
		} else if (!nodeName.equals(other.nodeName))
			return false;
		return true;
	}


}
