package pagerank;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;

/**
 * 
 * @author Vamsi Krishna J
 * @author Revanth A
 *
 */
public class PageRank {

	private Path filePath;
	private double approx;
	private WebGraph webGraph;
	private Map<String, Double> pageRank;

	public PageRank(String filePath, double approx) {
		this.filePath = Paths.get(filePath);
		this.approx = approx;
		this.webGraph = Util.constructWebGraphFromFile(this.filePath);
		this.pageRank = Util.computePageRanks(this.webGraph, this.approx);
		Util.updatePageRanksAsWeight(this.pageRank, this.webGraph);
	}

	public double pageRankOf(String nodeName) {
		return this.pageRank.get(nodeName);
	}

	public int outDegreeOf(String nodeName) {
		return webGraph.getNodesMap().getOrDefault(nodeName, new GraphNode("")).getEdges().size();
	}

	public int inDegreeOf(String nodeName) {
		return webGraph.getNodesMap().getOrDefault(nodeName, new GraphNode("")).getIndegree();
	}

	public int numEdges() {
		return (int) webGraph.getNodesMap().values().stream().mapToDouble(gn -> gn.getEdges().size()).sum();
	}

	public String[] topKPageRank(int k) {
		return pageRank.entrySet().stream().sorted(Map.Entry.<String, Double>comparingByValue().reversed()).limit(k)
				.map(Map.Entry::getKey).toArray(String[]::new);
	}

	public String[] topKInDegree(int k) {
		return webGraph.getNodesMap().values().stream()
				.sorted((gn1, gn2) -> Integer.compare(gn2.getIndegree(), gn1.getIndegree())).limit(k)
				.map(GraphNode::getNodeName).toArray(String[]::new);
	}

	public String[] topKOutDegree(int k) {
		return webGraph.getNodesMap().values().stream()
				.sorted((gn1, gn2) -> Integer.compare(gn2.getEdges().size(), gn1.getEdges().size())).limit(k)
				.map(GraphNode::getNodeName).toArray(String[]::new);
	}

}
