package pagerank;

/**
 * 
 * @author Vamsi Krishna J
 * @author Revanth A
 *
 */
public class WikiTennisRanker {
	public static void main(String[] args) {
		double[] approxs = new double[] { 0.01, 0.05 };
		for (double approx : approxs) {
			System.out.println("####### approx = " + approx + " #######");
			PageRank pageRank = new PageRank("/Users/krishnaj/Desktop/krishna/coursework/535/pa/pa3/WikiTennis.txt",
					approx);
			// Print Page Ranks for Top 10
			Util.printTopK(pageRank, 10);
			// Print Jaccard similarities between pairs
			Util.printJaccardSimilarity(pageRank, 100);
			System.out.println("######################");
		}

	}

}
