/**
 * This class is a data structure to implement in a WeightedQ.
 */
package pagerank;

import java.util.PriorityQueue;

/**
 * @author revanth
 * @author Vamsi Krishna Jagarlamudi
 *
 */

public class WeightedQ {
	/**
	 * Attributes in the class
	 */
	private PriorityQueue<GraphNode> weighted_queue = new PriorityQueue<GraphNode>();
	
	/**
	 * This function adds a GraphNode to the Queue if that elements is not present in the Queue already.
	 * @param t
	 */
	public void add(GraphNode t)
	{
		if(!weighted_queue.contains(t))
			weighted_queue.add(t);
	}
	
	/**
	 * This function returns a GraphNode from the Queue.This function internally uses the GraphNode's compareTo()
	 * function to compare the GraphNodes and return the appropriate GraphNode.
	 * @return
	 */
	public GraphNode extract()
	{
		GraphNode t = weighted_queue.poll();
		return t;
	}
	
	/**
	 * This function returns true if a GraphNode is already present in the Queue and false if it is not present in the Queue
	 * @param n
	 * @return
	 */
	public boolean contains(GraphNode n) {
		return weighted_queue.contains(n);
	}
}
