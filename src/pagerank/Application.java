/**
 * 
 */
package pagerank;

import java.net.MalformedURLException;

/**
 * @author revanth
 * @author Vamsi Krishna Jagarlamudi
 *
 */
public class Application {

	/**
	 * @param args
	 * @throws MalformedURLException 
	 */
	public static void main(String[] args) throws MalformedURLException {
		
		String[] topics = {"tennis", "grand slam"};
		WikiCrawler wc = new WikiCrawler("/wiki/tennis", topics, 1000, "xyz.txt", false);
		wc.crawl();
		
	}

}
